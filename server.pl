#!/usr/bin/perl -w -I.
#------------------------------------------------------
=head1 NAME

=head1 SYNOPSIS

=cut
#------------------------------------------------------
use strict;
use Socket;
use buckd;

#------------------------------------------------------
# null
#------------------------------------------------------
sub null {
    my ($par)=@_;

    return $par;
}
#------------------------------------------------------
# main
#------------------------------------------------------
sub main
{

     my $proto = getprotobyname('tcp');
     # create a socket, make it reusable
     socket(SOCKET, PF_INET, SOCK_STREAM, $proto)
        or die "Can't open socket $!\n";
     setsockopt(SOCKET, SOL_SOCKET, SO_REUSEADDR, 1)
        or die "Can't set socket option to SO_REUSEADDR $!\n";

     # bind to a port, then listen
     bind( SOCKET, pack_sockaddr_in($buckd::port, inet_aton($buckd::server)))
        or die "Can't bind to port $buckd::server : $buckd::port ! \n";

     listen(SOCKET, 5) or die "listen: $!";
     print "SERVER started on port $buckd::port\n";

     # accepting a connection
     my $client_addr;
     while ($client_addr = accept(NEW_SOCKET, SOCKET)) 
     {
        # send them a message, close connection
        my $name = buckd::sock_to_host(getpeername(*NEW_SOCKET));
        print "Connection recieved from ($name)\n";
        my $olds=select;

        buckd::serv(*NEW_SOCKET);

        select $olds;
        close NEW_SOCKET;
        print "Connection recieved from ($name) END\n";
     }

     return;
}
#------------------------------------------------------
main(@ARGV);
