#------------------------------------------------------
=head1 NAME

=head1 SYNOPSIS

=cut
#------------------------------------------------------
    no warnings 'uninitialized';
    use strict;

    package buckd;
#------------------------------------------------------
    use vars qw( $DIR $MYHOST $MYPORT $LOG $SERVER $server $port);

    $DIR = "./data";        # data dir
    $LOG = "./access.log";  # log file
    $MYHOST = "127.0.0.1";  # external (host) adress
    $MYPORT = "70";         # external port
    $SERVER = "";           # server name
    $server = $MYHOST;      # bind adress
    $port   = $MYPORT;      # bind port

    do("settings.pl");

    use vars qw( $request $dinfo $gplus $locator );

#------------------------------------------------------
# null
#------------------------------------------------------
    sub null {
        my ($par)=@_;


        return $par;
    }
#------------------------------------------------------
# printGlob
#------------------------------------------------------
    sub printGlob {
        my ($ds,$sel,$host, $port)=@_;

       	my $ditype = substr($ds, 0, 1);
       	my (@files) = sort(glob(($DIR.&canon($sel))));
       	foreach my $f (@files) {
       		next if ($f =~  m#/gophermap$# || m#/gophertag$# || $f =~ m#/\.\.?$# );

       		my ($itype,$slink)= &gen_itype($f,$locator);
       		$itype = $ditype if ($ditype ne '?');
       		$f = $slink if (length($slink));
       		$f =~ s/^$DIR//;

       		($f =~ m#/([^/]+)$#) && ($ds = $1);

       		print "$itype$ds\t$f\t$host\t$port\r\n";
       	}	

        return;
    }
#------------------------------------------------------
# sendCatalog
#------------------------------------------------------
    sub sendCatalog {

        my $dirname=$request;
        $dirname =~ s{/[^/]*$}{};

       	while(my $s=<S>) {
       		chomp($s);
       		if ($s =~ /^\./) {
       			$s = " ".$s;
       		}
       		if (! ($s =~ /\t/) ) {
       			$s ||= " ";
       			print "i$s\t\terror.host\t1\r\n";
       		} else {
       			my ($ds, $sel, $host, $port, $n) = split(/\t/, $s, 5);

       			$sel = substr($ds, 1) unless (length($sel) || length($host));
       			$sel = "$dirname/$sel" if ($sel !~ m#^/# && !length($host) &&
       				$sel !~ /^URL:/ && $sel !~ /^GET/);
       			unless ($sel =~ /^URL:/) {
       				1 while ($sel =~ s#//#/#);
       			}
       			$host ||= $MYHOST;
       			$port ||= $MYPORT;
       			if ( ($s =~ /^[^\t]+\t$/) ) {
       			        printGlob($ds,$sel,$MYHOST, $MYPORT);
       			} else {
       				print "$ds\t$sel\t$host\t$port\r\n";
       			}
       		}
       	}

        return;
    }
#------------------------------------------------------
# sendDirectory
#------------------------------------------------------
    sub sendDirectory {

        my @files=();

       	while(defined(my $entr = readdir(D))) {
       		next if ($entr =~ /^\./ ||
       			$entr eq 'gophertag' ||
       			$entr eq 'gophermap');
       		push(@files, $entr);
       	}
       	foreach my $entr (sort( @files )) {
       		my ($itype,$slink)=&gen_itype("$locator/$entr", $locator);
       		$slink ||= "$request/$entr";
       		$slink = &canon($slink);
       		print "$itype$entr\t$slink\t$MYHOST\t$MYPORT\r\n";
       	}


        return;
    }
#------------------------------------------------------
# sendText
#------------------------------------------------------
    sub sendText {

        my $len=0;
       	while(!eof(S)) {
       	        my $q="";
       		$len += read(S, $q, 16384);
       		$q =~ s/\r?\n/\r\n/sg;
       		print $q;
       	}
       	print "\r\n.\r\n";

        return $len;
    }
#------------------------------------------------------
# sendBin
#------------------------------------------------------
    sub sendBin {

        my $len=0;
       	while(!eof(S)) {
       	        my $q="";
       		$len += read(S, $q, 16384);
       		print $q;
       	}

        return $len;
    }
#------------------------------------------------------
# parseRequest
#------------------------------------------------------
    sub parseRequest {
        my ($orq)=@_;

        $orq =~ s/\r\n?$//;

       	($request, $dinfo) = split(/[\?\t]/, $orq, 2);
       	$request =~ s/%([a-fA-F0-9]{2})/pack("H2", $1)/eg;
       	# $request must be absolute ...
       	$request = "/$request" if ($request !~ m#^/#);
       	$request = &canon($request);

       	$gplus="";
       	if ($dinfo =~ m{^[\$\+\!]})
       	{
       	   $gplus="+";
       	   $dinfo=substr($dinfo,1);
       	   return;
       	}

       	# this is a kludge on %-escaping. try to see if needed first.
       	# otherwise arguments like +, % that aren't encoded from gopher
       	# clients will be eaten!
       	if ($dinfo =~ /%([a-fA-F0-9]{2})/) {
       		$dinfo =~ s/\+/ /g;
       		$dinfo =~ s/%([a-fA-F0-9]{2})/pack("H2", $1)/eg;
       	}
       	$locator = "$DIR$request";

        return;
    }
#------------------------------------------------------

#####################################################################
# Bucktooth 0.2.9 (c)1999-2011 Cameron Kaiser                       #
# All rights reserved.                                              #
# This is free software, but it is not GPL. Please see the License. #
#####################################################################

sub sock_to_host {
	my($sock) = (@_);
	return (undef, undef, undef) if (!$sock);
	my($sockaddr) = 'S n a4 x8';
	my($AFC, $port, $thataddr, $zero) = unpack($sockaddr, $sock);
	my($ip) = join('.', unpack("C4", $thataddr));
	my($af) = 2;

	my($hn) = $ip;

	return ($hn, $port, $ip);
}

#------------------------------------------------------
# serv
#------------------------------------------------------
    sub serv {
        local (*DATAS)=@_;

        my $isproxy=0;
        my $proxyip=0;
        my $proxyhost=0;
        my $proxyport=0;
        my $proxy=0;
        my $verbose=0;

        if ($isproxy) {
        	$ENV{'REMOTE_ADDR'} ||= $proxyip;
        	$ENV{'REMOTE_HOST'} ||= $proxyhost || $ENV{'REMOTE_ADDR'};
        	$ENV{'REMOTE_PORT'} ||= $proxyport;
        } else {
        	($ENV{'REMOTE_HOST'}, $ENV{'REMOTE_PORT'}, $ENV{'REMOTE_ADDR'}) =
        		&sock_to_host(getpeername(DATAS));
        }

        select(DATAS); $|++;

        while(1) {
        	my $grequest = ($isproxy) ? $proxy : <DATAS> ;
        	$grequest = pack("H".length($proxy), $proxy) if ($isproxy);

        	print STDOUT "[$grequest]\n";
        	parseRequest($grequest);
        	print STDOUT "[$request,$dinfo,$gplus,$locator]\n";
        	
        	if ($gplus) { # this is for UMN gopher
        		my $j = <<"EOF";
+-1
+INFO: 1Main menu (non-gopher+)		$MYHOST	$MYPORT
+ADMIN:
 Admin: Server Administrator
 Server: $SERVER
+VIEWS:
 application/gopher+-menu: <512b>
+ABSTRACT:
 This gopher supports standard gopher access only. Use this
 kludge to disable gopher+ client requests by your client.
.
EOF
        		$j =~ s/\n/\r\n/sg;
        		print $j;
        		&log("\"$request\" <$dinfo> [300 \"\" forced g+ redirect]");
        		return;
        	}
        # quux.org/0/Archives/Mailing Lists/gopher/gopher.2002-02?/MBOX-MESSAGE/34
        	if ($request =~ s#^/?URL:([^:/]+:/?/?)#$1#) {
#        		$request =~ s/\t/?/;

        		print <<"EOF";
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<html>
<head>
<meta http-equiv="refresh" content="5;URL=$request">
<title>SaveGopher Redirect to URL: $request</title>
</head>
<body>
<p>
You are following a link from gopher to another URL or protocol. You
will be automatically taken to the site shortly.  If you don't get
sent there, please click <a href="$request">here</a> to go to the site.
</p>
<p>
The URL linked is:
</p>
<p>
<a href="$request">$request</a>
</p>
<hr>
<address>generated by SaveGopher 1.0.6</address>
</body>
</html>
EOF

        		&log("\"$request\" ${dinfo}[300 \"\" URL redirect]");
        		return;
        	}


        	# load gophermap info
        	if (-d $locator) {
        		if (-x "$locator/gophermap") {
        			$locator .= "/gophermap";
#        			&mole;
        		}
        		if (sysopen(S, "$locator/gophermap", 0)) {
                                sendCatalog();
        			close(S);
        		} else {

        			if (!opendir(D, $locator)) {
	        			&errorr(403, "isn't accessible to you"
					, "This resource's permissions do not permit you to read it.");
					return;
				}

                                sendDirectory();

        			closedir(D);
        		}
        		print ".\r\n";
        		&log("\"$request\" ${dinfo}[200 \"$locator\" directory]");

        		return;
        	} else {
        		if (-x $locator  && ! -d $locator) {
#        			&mole;
        		} else {
        			if(!sysopen(S, $locator, 0)) {
        				&errorr(404, "doesn't exist", "This resource cannot be located.");
                                        return;
        			}
        			my $typeExt=&itypebyext($locator);
                		my $len = 0;
        			if ( $typeExt eq "1")
        			{
                                        sendCatalog();
        			}
        			elsif ( $typeExt eq "0")
        			{
        			     $len = sendText(); 
        			}
        			else
        			{
        			     $len = sendBin(); 
        			}
        			close(S);

        			&log(sprintf("\"%s\" %s[201 \"%s\" %i bytes]", $request
        			, (($dinfo) ? "<$dinfo> " : ""), $locator, $len));

        			return;
        		} 
        	}
        	&errorr(500, "makes me go wicked", "A system error has occurred");
        	return;
        }

        return;
    }
#------------------------------------------------------

sub errorr {
	my ($code, $emsg1, @stuff) = (@_);

	print  "3 '$request' $emsg1!\t\terror.host\t1\r\n";
	if (@stuff) {
		foreach (@stuff) {
			print "i $_\t\terror.host\t1\r\n";
		}
	}
	print ".\r\n";
	&log("\"$request\" ${dinfo}[$code \"$DIR$request\" error \"$emsg1\"]");
	return;
}

sub log {
	my ($crap) = "@_";
	$crap =~ s/\t/\?/; # just the first one
	open(Q, ">>$LOG") &&
		printf(Q "%s %s %s\n", scalar localtime, $crap, $ENV{'REMOTE_HOST'} || $ENV{'REMOTE_ADDR'}) &&
		close(Q);
}

sub gen_itype {
	my($pentr, $loctr) = (@_);

	my $xentr = $pentr;
	my $itype = &itypebyext($xentr);
	$xentr =~ s/^$DIR//;
	return ($itype, ($pentr eq $xentr) ? '' : $xentr);
}


sub itypebyext {
	my ($xentr) = (@_);
	my $itype =
		(-d $xentr) ? "1" :
		($xentr =~ /\.gopher$/i) ? "1" :
		($xentr =~ /\.txt$/i) ? "0" :
		($xentr =~ /\.gif$/i) ? "g" :
		($xentr =~ /\.gz$/i) ? "9" :
		($xentr =~ /\.zip$/i) ? "5" :
		($xentr =~ /\.jpe?g$/i) ? "I" :
		($xentr =~ /\.png$/i) ? "p" :
		($xentr =~ /\.pdf$/i) ? "d" :
		($xentr =~ /\.css$/i) ? "c" :
		($xentr =~ /\.xml$/i) ? "x" :
		($xentr =~ /\.html?$/i) ? "h" :
		($xentr =~ /\.hqx$/i) ? "4" :
		(-B $xentr) ? "9" :
	"0";
	return $itype;
}

sub mole_ {
	$ENV{'SERVER_HOST'} = $MYHOST;
	$ENV{'SERVER_PORT'} = $MYPORT;
	my $orq =~ s/\t/?/;
	$ENV{'SELECTOR'} = $orq;
	$ENV{'GPLUS'} = $gplus;
	&fixgplus;
	&log("\"$orq\" ${gplus}[200 \"$locator\" \"$dinfo\" executed]");
	$ENV{'REQUEST'} = $request;
	if (defined($dinfo)) {
		exec($locator, split(/ /, $dinfo)); 
	} else {
		exec($locator);
	}
	&errorr(500, "wicked mole: $!\n");
}

sub canon {
	my ($request) = (@_);
	1 while $request =~ s#//#/#;
        1 while $request =~ s#/\.(/|$)#$1#;
        1 while $request =~ s#/[^/]*/\.\.(/|$)#$1#;
        1 while $request =~ s#^/\.\.(/|$)#$1#;
	return $request;
}

#------------------------------------------------------
1;

